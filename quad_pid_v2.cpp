#include "ros/ros.h"
#include "geometry_msgs/Point.h" //library for waypoint message object
#include "geometry_msgs/PoseStamped.h" // library for position message object
#include <std_msgs/Int16.h>
#include <std_msgs/Float32.h>
#include <math.h>
#include <tf/transform_datatypes.h>

using namespace std;

#define THROTTLE_MAX 75
#define YAW_MAX 100
#define PITCH_MAX 100
#define ROLL_MAX 100

float loop_time = 0.1; // In seconds
float wp_x = 1.0;
float wp_y = 0.0;
float wp_z = 1.0;
float wp_angle_x = 0.0;
float wp_angle_y = 0.0;
float wp_angle_z = 0.0;
float e_z = 0.0;
float e_yaw = 0.0;
float e_x_body = 0.0;
float e_x = 0.0;
float e_y_body = 0.0;
float e_y = 0.0;
float u_float = 0.0;

float throttle_hover = 70.0;

float kp_throttle = 10.0;
float ki_throttle = 0.0;

float throttle_p_term = 1.0;
float throttle_i_term = 0.0;
float kp_yaw = 10.0;
float kp_roll = 10.0;
float ti_roll = 0.0;
float td_roll = 0.0;
float kp_pitch = 10.0;
float ti_pitch = 0.0;
float td_pitch = 0.0;

double roll_m, pitch_m, yaw_m; // Measured pose in radians

float x_desired = 0.0; // Desired x position
float y_desired = 0.0; // Desired y position
float x_measured = 0.0; // X position from qualisys
float y_measured = 0.0; // Y position from qualisys

std_msgs::Int16 throttle;
std_msgs::Int16 roll;
std_msgs::Int16 pitch;
std_msgs::Int16 yaw;


void waypoint_messageCb(const geometry_msgs::Point quad_waypoint) //Receiving the waypoint coordinates
{
  wp_x = quad_waypoint.x;
  wp_y = quad_waypoint.y;
  wp_z = quad_waypoint.z;
}


void position_messageCb(const geometry_msgs::PoseStamped quad_position) //Receiving position and calculating error
{ 
 
  // Convert quaternion matrix to radian matrix
  tf::Quaternion q(quad_position.pose.orientation.x, quad_position.pose.orientation.y, quad_position.pose.orientation.z, quad_position.pose.orientation.w);
  tf::Matrix3x3 m(q);
  m.getRPY(roll_m, pitch_m, yaw_m);
  // std::cout << "Roll: " << roll_m << ", Pitch: " << pitch_m << ", Yaw: " << yaw_m << std::endl; // DEBUG
  
  
  
  // ** Calculate error in x, y, z direction ** //
  
  x_measured = quad_position.pose.position.x; // get x and y position from qualisys
  y_measured = quad_position.pose.position.y;
  
  e_x = (wp_x - x_measured); // Error x in origo frame
  e_y = (wp_y - y_measured); // Error y in origo frame
  e_z = wp_z - quad_position.pose.position.z; // Error z-axis

  e_x_body = e_x * cos(yaw_m) + e_y * sin(yaw_m);
  e_y_body = -e_x * sin(yaw_m) + e_y * cos(yaw_m);

  //std::cout << "e_x_body: " << e_x_body << ", e_y_body: " << e_y_body << std::endl; // DEBUG

  //cout << "e_z = " << e_z << "\n";




  // ** THROTTLE PID ** //

  throttle_p_term = kp_throttle * e_z;
  throttle_i_term += e_z * loop_time;
      
  if(throttle_i_term > THROTTLE_MAX * 0.2){ // Integral
    throttle_i_term = THROTTLE_MAX * 0.2;
  }
  else if (throttle_i_term <= 0.0){
    throttle_i_term = 0.0;
  }
  cout << "throttle_i_term = " << throttle_i_term << "\n";
  
  throttle.data = (int)throttle_p_term + ki_throttle * throttle_i_term + throttle_hover;
  
  // Set max/min throttle
  if(throttle.data > THROTTLE_MAX){ // Total throttle
    throttle.data = THROTTLE_MAX;
  }
  else if (throttle.data < 0){
    throttle.data = 0;
  }
  cout << "throttle.data = " << throttle.data << "\n";




  // ** ROLL PID ** //

  roll.data = (int)(kp_roll * e_y_body + 50.0);

  // Set max/min roll
  if(roll.data > ROLL_MAX)
    roll.data = ROLL_MAX;
  else if(roll.data <= 0.0)
    roll.data = 0.0;



  // ** PITCH PID ** //

  pitch.data = (int)(kp_pitch * e_x_body + 50.0);

  // Set max/min pitch
  if(pitch.data > PITCH_MAX)
    pitch.data = PITCH_MAX;
  else if(pitch.data <= 0.0){
    pitch.data = 0.0;
  }



  // ** YAW PID ** //
  
  e_yaw = wp_angle_z - yaw_m;
  
  yaw.data = (int)(kp_yaw * e_yaw );

  // Set max/min yaw
  if(yaw.data > YAW_MAX)
    yaw.data = YAW_MAX;
  else if(yaw.data <= -YAW_MAX){
    yaw.data = -YAW_MAX;
  }


  std::cout << "roll: " << roll.data << ", pitch: " << pitch.data <<  ", yaw: " << yaw.data << std::endl; // DEBUG





  /*//YAW PID
  e_yaw = wp_angle_z - quad_position.pose.orientation.z;
  yaw.data = (int)kp_yaw * e_yaw;
  
  if(yaw.data > YAW_MAX)
    yaw.data = YAW_MAX;
  else if (yaw.data < 0)
    yaw.data = -YAW_MAX;
  
  //ROLL PID




  /*e_roll = wp_x - quad_position.pose.position.x;
  roll.data = (int)kp_roll * e_roll;
  
  if(roll.data > ROLL_MAX)
    roll.data = ROLL_MAX;
  else if(roll.data < 0)
    roll.data = - ROLL_MAX;
  
  //PITCH PID
  
  e_x_body = wp_y - quad_position.pose.position.y;
  pitch.data = (int)kp_pitch * e_x_body;
  
  if(pitch.data > PITCH_MAX)
    pitch.data = PITCH_MAX;
  else if(pitch.data < 0){}
    pitch.data = -PITCH_MAX;
  */
}

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "quad_PID");
  
  ros::NodeHandle n;
  
  ros::Publisher throttle_pub = n.advertise<std_msgs::Int16>("quad_throttle", 1000);
  ros::Publisher roll_pub = n.advertise<std_msgs::Int16>("quad_roll", 1000);
  ros::Publisher pitch_pub = n.advertise<std_msgs::Int16>("quad_pitch", 1000);
  ros::Publisher yaw_pub = n.advertise<std_msgs::Int16>("quad_yaw", 1000);
  ros::Subscriber quad_sub_position = n.subscribe("qualisys/quadcopter/pose", 1000, position_messageCb);
  ros::Subscriber quad_sub_waypoint = n.subscribe("waypoint", 1000, waypoint_messageCb);
  
  ros::Rate loop_rate(1/loop_time);

while (ros::ok())
{
  
  throttle_pub.publish(throttle);
  roll_pub.publish(roll);
  pitch_pub.publish(pitch);
  yaw_pub.publish(yaw);
  
  ros::spinOnce();
  loop_rate.sleep();
}


  return 0;
}
