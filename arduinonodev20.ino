#include <ros.h>
//#include <std_msgs/String.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Bool.h>

#define THROTTLE_PIN 9 // o-100%
#define YAW_PIN 6 // Middle value: 50% (mapped)
#define PITCH_PIN 11 // Middle value: 50% (mapped)
#define ROLL_PIN 10 // Middle value: 50% (mapped)

ros::NodeHandle  nh;
bool armed_previous = 0;
std_msgs::Int16 throttle_received;
std_msgs::Int16 yaw_received;
std_msgs::Int16 pitch_received;
std_msgs::Int16 roll_received;
std_msgs::Bool Arm_received;

void throttle_messageCb (const std_msgs::Int16& quad_throttle)
{
 // throttle = quad_throttle.data;
   analogWrite(9, map(quad_throttle.data, 0, 100, 0, 170));
}
void pitch_messageCb (const std_msgs::Int16& quad_pitch)
{
 // throttle = quad_throttle.data;
   analogWrite(11, map(quad_pitch.data, 0, 100, 0, 170));
}
void roll_messageCb (const std_msgs::Int16& quad_roll)
{
 // throttle = quad_throttle.data;
   analogWrite(10, map(quad_roll.data, 0, 100, 0, 170));
}
void yaw_messageCb (const std_msgs::Int16& quad_yaw)
{
 // throttle = quad_throttle.data;
   analogWrite(6, map(quad_yaw.data, 0, 100, 0, 170));
}
void arm_messageCb (const std_msgs::Bool& quad_armed)
{
   if (!quad_armed.data && armed_previous) //Disarmed
   {
    analogWrite(THROTTLE_PIN, 0);
    analogWrite(YAW_PIN, 170);
    armed_previous = 0;
    delay(2000);
    analogWrite(YAW_PIN, 85);
   }
   if (quad_armed.data && !armed_previous) //Armed
   {
    analogWrite(THROTTLE_PIN, 0);
    analogWrite(YAW_PIN, 0);
    armed_previous = 1;
    delay(2000);   
    analogWrite(YAW_PIN, 85); 
   }
}

ros::Subscriber<std_msgs::Int16> quad_sub_throttle("quad_throttle", &throttle_messageCb);
ros::Subscriber<std_msgs::Int16> quad_sub_pitch("quad_pitch", &pitch_messageCb);
ros::Subscriber<std_msgs::Int16> quad_sub_roll("quad_roll", &roll_messageCb);
ros::Subscriber<std_msgs::Int16> quad_sub_yaw("quad_yaw", &yaw_messageCb);
ros::Subscriber<std_msgs::Bool> quad_sub_arm("quad_arm", &arm_messageCb);

void setup()
{ 
  analogWrite(9, 0);
  analogWrite(10, 85);
  analogWrite(11, 85);
  analogWrite(6, 85);
  nh.initNode();
  nh.subscribe(quad_sub_throttle);
  nh.subscribe(quad_sub_pitch);
  nh.subscribe(quad_sub_roll);
  nh.subscribe(quad_sub_yaw);
  nh.subscribe(quad_sub_arm);
}

void loop()
{  
  nh.spinOnce();
  delay(1);
}
