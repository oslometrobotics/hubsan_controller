#include <ros.h>
#include <std_msgs/String.h>

ros::NodeHandle  nh;
std_msgs::String message_received;
String message = "empty";
char *message_char = "empty";
void messageCb( const std_msgs::String& arduino_message)
{
  message = arduino_message.data;
  message_char = &message[0u];
    if(message == "led_on")
  {
     digitalWrite(13, HIGH);
  }
  if(message == "led_off")
  {
    digitalWrite(13, LOW);
  }
}

ros::Subscriber<std_msgs::String> sub("arduino_message", &messageCb );
ros::Publisher confirm("confirm", &message_received);

void setup()
{ 
  pinMode(13, OUTPUT);
  nh.initNode();
  nh.subscribe(sub);
  nh.advertise(confirm);
  
}

void loop()
{  
  message_received.data = message_char;
  confirm.publish(&message_received);
  nh.spinOnce();
  delay(1);
}

