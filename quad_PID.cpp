#include "ros/ros.h"
#include "geometry_msgs/Point.h" //library for waypoint message object
#include "geometry_msgs/PoseStamped.h" // library for position message object
#include <std_msgs/Int16.h>

using namespace std;

#define THROTTLE_MAX 75
#define YAW_MAX 100
#define PITCH_MAX 100
#define ROLL_MAX 100

  float wp_x = 0.0;
  float wp_y = 0.0;
  float wp_z = 0.0;
  float wp_angle_x = 0.0;
  float wp_angle_y = 0.0;
  float wp_angle_z = 0.0;
  float e_throttle = 0.0;
  float e_yaw = 0.0;
  float e_pitch = 0.0;
  float e_roll = 0.0;
  float u_float = 0.0;

  float kp_throttle = 20.0;
  float ki_throttle = 1.0;
  float kp_yaw = 2.0;
  float kp_roll = 2.0;
  float kp_pitch = 2.0;
  float throttle_p_term = 1.0;
  float throttle_i_term = 0.0;


  std_msgs::Int16 throttle;
  std_msgs::Int16 roll;
  std_msgs::Int16 pitch;
  std_msgs::Int16 yaw;

  void waypoint_messageCb(const geometry_msgs::Point quad_waypoint) //Receiving the waypoint coordinates
  {
    wp_x = quad_waypoint.x;
    wp_y = quad_waypoint.y;
    wp_z = quad_waypoint.z;
  }
  void position_messageCb(const geometry_msgs::PoseStamped quad_position) //Receiving position and calculating error
  {


    //THROTTLE PID
    e_throttle = wp_z - quad_position.pose.position.z;
    cout << "e_throttle = " << e_throttle << "\n";
    throttle_p_term = kp_throttle * e_throttle;
    throttle_i_term += e_throttle;
    if(throttle_i_term > 80){
        throttle_i_term = 80;
    }
    else if(throttle_i_term < 40){
        throttle_i_term = 40;
    }
    cout << "throttle_i_term = " << throttle_i_term << "\n";

    throttle.data = (int)throttle_p_term + ki_throttle * throttle_i_term;


    if(throttle.data > THROTTLE_MAX){
      throttle.data = THROTTLE_MAX;
      }
    else if (throttle.data < 0){
      throttle.data = 0;
      }
      cout << "throttle.data = " << throttle.data << "\n";



    //YAW PID
    e_yaw = wp_angle_z - quad_position.pose.orientation.z;
    yaw.data = (int)kp_yaw * e_yaw;
    
    if(yaw.data > YAW_MAX)
      yaw.data = YAW_MAX;
    else if (yaw.data < 0)
      yaw.data = -YAW_MAX;

    //ROLL PID
    e_roll = wp_x - quad_position.pose.position.x;
    roll.data = (int)kp_roll * e_roll;

    if(roll.data > ROLL_MAX)
      roll.data = ROLL_MAX;
    else if(roll.data < 0)
      roll.data = - ROLL_MAX;

    //PITCH PID
    e_pitch = wp_y - quad_position.pose.position.y;
    pitch.data = (int)kp_pitch * e_pitch;

    if(pitch.data > PITCH_MAX)
      pitch.data = PITCH_MAX;
    else if(pitch.data < 0){}
      pitch.data = -PITCH_MAX;

  }

int main(int argc, char **argv)
{

  ros::init(argc, argv, "quad_PID");

  ros::NodeHandle n;

  ros::Publisher throttle_pub = n.advertise<std_msgs::Int16>("quad_throttle", 1000);
  ros::Publisher roll_pub = n.advertise<std_msgs::Int16>("quad_roll", 1000);
  ros::Publisher pitch_pub = n.advertise<std_msgs::Int16>("quad_pitch", 1000);
  ros::Publisher yaw_pub = n.advertise<std_msgs::Int16>("quad_yaw", 1000);
  ros::Subscriber quad_sub_position = n.subscribe("qualisys/quadcopter/pose", 1000, position_messageCb);
  ros::Subscriber quad_sub_waypoint = n.subscribe("waypoint", 1000, waypoint_messageCb);

  ros::Rate loop_rate(10);

  while (ros::ok())
  {

    throttle_pub.publish(throttle);
    roll_pub.publish(roll);
    pitch_pub.publish(pitch);
    yaw_pub.publish(yaw);

    ros::spinOnce();
    loop_rate.sleep();
  }


  return 0;
}
